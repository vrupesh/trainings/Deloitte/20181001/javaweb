package com.guruofjava.deloitte2.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Objects;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddMemberServlet
 */
@WebServlet("/registerMember")
public class AddMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String email = request.getParameter("mail");
		
		Connection con = null;
		PreparedStatement pst = null;
		
		PrintWriter out = response.getWriter();
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "kumar", "kumar");
			pst = con.prepareStatement("INSERT INTO members VALUES(member_sequence.nextval, ?, ?)");
			
			pst.setString(1, name);
			pst.setString(2, email);
			
			pst.executeUpdate();
			
			//out.println("<html><body><h2>Successfully registered.</h2></body></html>");
			
			RequestDispatcher rd = request.getRequestDispatcher("/viewMembers");
			rd.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace(out);
		} finally {
			if (Objects.nonNull(con)) {
				try {
					con.close();
				} catch (Exception e) {

				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
