package com.guruofjava.deloitte2.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns= {"/viewMembers"})
public class ViewMembersServlet extends GenericServlet {
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "kumar", "kumar");
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM members");

			if (rs.next()) {
				out.println("<table border='1' cellpadding='2' cellspacing='3'>"
						+ "<tr><th>Member ID</th><th>Name</th><th>Email</th></tr>");

				do {
					out.println("<tr>" + "<td>" + rs.getInt(1) + "</td>" + "<td>" + rs.getString(2) + "</td>" + "<td>"
							+ rs.getString(3) + "</td>" + "</tr>");
				} while (rs.next());

				out.println("</table>");
			}

		} catch (Exception e) {
			e.printStackTrace(out);
		} finally {
			if (Objects.nonNull(con)) {
				try {
					con.close();
				} catch (Exception e) {

				}
			}
		}
	}
}
